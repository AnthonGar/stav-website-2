﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="StavProject.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome Page</title>

    <link rel="stylesheet" href="/CSS/nav-bar.css" />
    <link rel="stylesheet" href="/CSS/scroller.css" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet" />

</head>
<body>
    <div class="topnav">
        <a class="active" href="#home">Home</a>
        <a href="Login.aspx">Login</a>
        <a href="Register.aspx">Register</a>
        <asp:HyperLink ID="HyperLink1" runat="server">HyperLink</asp:HyperLink>
        <a href="#Activites">Activities</a>
        <a href="#about">About</a>
    </div>
</body>
</html>
