﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StavProject.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="login_form" runat="server">
        <asp:TextBox ID="txt_username" placeholder="Type your username" class="input-txt-box" runat="server"></asp:TextBox>
        <asp:TextBox ID="txt_password" TextMode="Password" placeholder="Type your password" class="input-txt-box" runat="server"></asp:TextBox>
        <asp:Button ID="btn_login" runat="server" Text="Submit" class="input-btn" OnClick="btn_login_Click" />
    </form>
</body>
</html>
