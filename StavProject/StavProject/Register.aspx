﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="StavProject.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register Page</title>
</head>
<body>
    <form id="register-form">
        <label>Username: </label>
        <input type="text" id="username-input" placeholder="Enter Username Here" required="required" /> <!-- Register Form -->
        <label>Email:</label>
        <input type="text" id="email-input" required="required" />
        <label>Password:</label>
        <input type="text" id="password-input" required="required" />
        <label>Confirm Password:</label>
        <input type="text" id="confirm-password-input" required="required" />

        <select id='pref'>
            <option value="bikes">
                Biking Accross New-York
            </option>
            <option value="food">
                Ultimate Cooking Experience 
            </option>
            <option value="viewing">
                Site-Seeing Accross New-York
            </option>
        </select>

        <input type="submit" id="submit-btn" />

        <ul class="error-list" id="error-list">
        </ul>
    </form>
</body>

<script>
    /* const myForm = document.getElementById("register-form");

    myForm.addEventListener("submit", (e) => {
        e.preventDefault();

        const request = new XMLHttpRequest();
        request.open("post", "");
        request.onload = () => {
            console.log(request.responseText);
        };

        request.send(new FormData(myForm));
    }); */

    function AddError(data, list) {
        var list_item = document.createElement("li");
        var node = document.createTextNode(data);
        list_item.appendChild(node);

        list.appendChild(list_item);
    }
    

    function ValidateEmail() {
        const email = document.getElementById("email-input").value;
        const index_of_amp = email.indexOf('@');

        var list_item;
        var data;
        var error_list = document.getElementById("error-list");

        // Removing all errors from before.
        while (error_list.firstChild) {
            error_list.removeChild(error_list.firstChild)
        }

        if (!(email.length > 7 && email.length < 32)) {
            //Length Error
            AddError("E-mail must be 7-32 chars long.", error_list);
        }
        if (!(index_of_amp != -1)) {
            //Missing @
            AddError("E-mail is missing @.", error_list);
        }
        if (!(index_of_amp != -1 && index_of_amp < email.lastIndexOf('.'))) {
            //Missing TLD
            AddError("E-mail is missing TLD (.com/.il/.gov).", error_list);
        }
    }

</script>
</html>
